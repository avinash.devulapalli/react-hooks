import React from 'react';
import './App.css';

import ClassCounter from './components/ClassCounter';
import HookCounter from './components/HookCounter';
import HookCounterTwo from './components/HookCounterTwo';
import HookCounterThree from './components/HookCounterThree';
import HookCounterFour from './components/HookCounterFour';
import HookCounterOne from './components/HookCounter1';

function App() {
  return (
    <div className="App">
      <ClassCounter></ClassCounter>
      <HookCounter></HookCounter>
      <HookCounterTwo></HookCounterTwo>
      <HookCounterThree></HookCounterThree>
      <HookCounterFour></HookCounterFour>
      <HookCounterOne></HookCounterOne>
    </div>
  );
}

export default App;
